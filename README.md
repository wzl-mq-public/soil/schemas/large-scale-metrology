# Large-Scale Metrology
Schema for modeling a Large-Scale Metrology Device in SOIL

[**Interactive View**](https://git-ce.rwth-aachen.de/wzl-mq-public/soil/schemas/large-scale-metrology/-/blob/main/public/openapi.yml)

![Overview](./media/LSM-System-Model.png)

## Viewing
To view the schema, you can use any tool that supports the OpenAPI specification and open the bundled file at [public/openapi.yml](https://git-ce.rwth-aachen.de/wzl-mq-public/soil/schemas/large-scale-metrology/-/blob/main/public/openapi.yml).

This also works with GitLab's integrated Swagger-Editor.

## Contributing
To contribute, you can use any editor and edit the files in the folder *src*. We recommend Visual Studio Code with the OpenAPI extension.
Please adhere to the filestructure which decomposes resources into individual files.

## Bundling
To bundle the files to the public `openapi.yml` file, you can use `swagger-cli`:
```
npm install -g swagger-cli
npx swagger-cli bundle .\src\openapi.yml -o .\public\openapi.yml -r -t yaml
```
