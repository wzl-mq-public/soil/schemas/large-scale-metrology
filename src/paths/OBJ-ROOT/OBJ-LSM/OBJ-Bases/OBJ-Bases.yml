description: >
  **Base Stations**  

  Object maintaining a list of all base stations within the Large-Scale Metrology System.
  In case of a centralized system, e.g. a laser tracker, this is exactly 1. In case of a distributed
  system, each node should have an entry here. This is also the entry point to register a new base stations.
  Manual insertion of base stations must be supported and implemented by the system itself.

get:
  summary: READ
  tags:
    - LSM
  description:
    $ref: '#/description'
  responses:
    '200':
      description: >
        **OK**  

        Everything occurred as expected.
      content:
        application/json:
          schema: 
            $ref: '../../../../schemas/index.yml#/Object'
          example:
            uuid: OBJ-Bases
            name: Bases
            description: Base Stations of the Large-Scale Metrology System.
            ontology: null
            children:
              - uuid: OBJ-A
                name: Base Station A
              - uuid: OBJ-B
                name: Base Station B
    '500':
      $ref: '../../../../responses/index.yml#/InternalError'

put:
  summary: CREATE
  tags:
    - LSM
  description:
    $ref: '#/description'
  requestBody:
    content:
      application/json:
        schema:
          type: object
          properties:
            uuid:
              type: string
              pattern: '^OBJ-[a-zA-Z0-9_]+'
              description: Unique identifier of the base station
            name:
              type: string
              description: Name of the base station
            description:
              type: string
              description: Description of the base station
        example:
          uuid: OBJ-C
          name: Base Station C
          description: Base Station C of the Large-Scale Metrology System.

  responses:
    '201':
      description: >
        **CREATED**  

        The new base station was successfully created and can now be retrieved using its uuid.
    '405':
      description: >
        **NOT ALLOWED**  

        This system does not allow manual addition of base stations.
    '500':
      $ref: '../../../../responses/index.yml#/InternalError'