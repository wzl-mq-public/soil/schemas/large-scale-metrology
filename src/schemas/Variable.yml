type: object
properties:
  uuid:
    type: string
    pattern: '^VAR-[a-zA-Z0-9_]+'
    description: Unique identifier of the variable.
  name:
    type: string
    description: Human-readable name.
  description:
    type: string
    description: Human-readable description.
  ontology:
    type: string
    nullable: true
    description: Reference to a SOIL ontology. Reserved for future use.
  datatype:
    type: string
    enum: [int, double, string, bool, time, enum]
    description: >
      Datatype identifier for values:
        * `int` - Integer
        * `double` - Double, this is the only floating type supported
        * `string`- String
        * `bool`- Boolean
        * `time`- Time, represented as RFC3339
        * `enum` - enumeration, a constrained set of strings
  dimension:
    type: array
    items:
      type: integer
    description: Dimensions of the variable, can be [] for scalar
  unit:
    type: string
    nullable: true
    description: >
      UNECE Code stating the unit of the value. 
      A list is available at [https://tfig.unece.org/contents/recommendation-20.htm](https://tfig.unece.org/contents/recommendation-20.htm)
  range:
    type: array
    items:
      oneOf:
      - type: string
      - type: integer
      - type: number
        format: double
      - type: string
        format: date-time
    description: >
      Acceptable range of the value. [] means no restrictions. If not [], minium and maximum (i.e. array of length 2)
      must always be provided. For numerical values, the definition is trivial. For strings, the limit must be integers
      imposing length restrictions. For times, a lower and upper boundary can be passed in RFC3339 format. For enumerations,
      the range is the list of applicable choice. The range check is expected to be inclusive.
  value:
    nullable: true
    description: Value of the variable expressed in the according datatype.
  covariance:
    oneOf:
      - type: number
        format: double
      - type: array
        items:
          type: array
          items:
            type: number
            format: double
    nullable: true
    description: > 
      (Co-)Variance as scalar value or matrix expressed in the same units as value.
      It can be set to *null* if no covariance estimate is available. Currently only
      the double data type is supported here.
  timestamp:
    type: string
    format: date-time
    nullable: true
    description: >
      Timestamp attributed to setting the value in RFC3339 format.
  nonce:
    type: string
    nullable: true
    description: >
      Arbitrary string that can be set by the user to link the measurement
      to a context, e.g. an experiment or batch identifier.
  hash:
    type: string
    pattern: '([0-9a-fA-F]{2} )*([0-9a-fA-F]{2})'
    nullable: true
    description: >
      Hexadecimal representation of hash providing the ability for integrity checks.
      This can either be a SHA256 of a defined, byte-wise variable serialization or
      a signature of the SHA256 hash if the device possesses valid private key.